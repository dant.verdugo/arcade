var staticCacheName = 'arcade-pwa';
var filesToCache = [
    '/',
    '/home',
    '/gallery',
    '/gallery-card',
    '/contact',
    '/static/css/bootstrap/bootstrap-grid.css',
    ]

self.addEventListener('install', function(event) {
    console.log('registrando serviceworker');
    event.waitUntil(
        caches.open(staticCacheName).then(function(cache) {
            return cache.addAll(filesToCache);
        })
    );
});

self.addEventListener('fetch', function(event) {
    var requestUrl = new URL(event.request.url);
    /*if (requestUrl.origin === location.origin) {
        if ((requestUrl.pathname === '/')) {
            console.log(caches)
            event.respondWith(caches.match(request));
            return;
        }
        if ((requestUrl.pathname === '/home')) {
            event.respondWith(caches.match('/home'));
            return;
        }
        if ((requestUrl.pathname === '/gallery')) {
            event.respondWith(caches.match('/gallery'));
            return;
        }
        if ((requestUrl.pathname === '/gallery-card')) {
            event.respondWith(caches.match('/gallery-card'));
            return;
        }
        if ((requestUrl.pathname === '/contact')) {
            event.respondWith(caches.match('/contact'));
            return;
        }
    }
    event.respondWith(
        caches.match(event.request).then(function(response) {
            return response || fetch(event.request);
        })
    );*/
    /*event.respondWith(caches.match(event.request).then(function(response) {        
        if (response !== undefined) {
            console.log('respuesta no definida, cargado desde la cache ->  -> ' + event.request.url);
            return response;
        } else {                   
            return fetch(event.request).then(function(response) {
                console.log('recurso clonado en cache ->' + event.request.url);
                let responseClone = response.clone();

                caches.open(staticCacheName).then(function(cache) {
                    cache.put(event.request, responseClone);
                });
                return response;
            }).catch(function() {                       
                console.log('recurso cargado desde la cache -> ' + event.request.url);
                var requestUrl = new URL(event.request.url);
                return caches.match(requestUrl.pathname);
            });
        }
    }));*/
});